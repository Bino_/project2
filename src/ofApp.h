#pragma once

#include "ofMain.h"
#include "ofxRunway.h"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		ofxRunway runwayDeeplab, runwayCoco;
		ofImage startingImage, DeeplabImage, CocoImage;

		// Callback functions that process what Runway sends back
		void runwayInfoEvent(ofJson& info);
		void runwayErrorEvent(string& message);

};
