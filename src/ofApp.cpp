#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	//setup runway
	bool dl = runwayDeeplab.setup("http://localhost:8000");//DeepLab
	runwayDeeplab.start();
	bool cc = runwayCoco.setup("http://localhost:8001");//SPADE-COCO
	runwayCoco.start();
	//setup images
	struct ofImageLoadSettings s;//when looking up load the documentation said I need one of these I couldn't figure out how to make it work 
	s.accurate = true;
	bool i = startingImage.load("images/image2.JPG", s);//load in a jpg from images folder. this doesn't work i'm not sure why
	DeeplabImage.allocate(startingImage.getHeight(), startingImage.getWidth(), OF_IMAGE_COLOR);
	CocoImage.allocate(startingImage.getHeight(), startingImage.getWidth(), OF_IMAGE_COLOR);
	std::cout << "deeplab: " << dl << " coco: " << cc << " image2: " << i << "\n";//making sure deeplab, coco and the jpg image get set up correctly
}

//--------------------------------------------------------------
void ofApp::update(){
	
	if (!runwayDeeplab.isBusy() && !runwayCoco.isBusy()) {//if deeplab isn't busy and coco isn't busy
		runwayDeeplab.send("image", startingImage);//send the JPG to Deepmind
		runwayDeeplab.get("image", DeeplabImage);//get semantic map
		ofxRunwayData data;
		data.setImage("semantic_map", DeeplabImage, OFX_RUNWAY_JPG);//setup semantic map to send to coco
		runwayCoco.send(data);//send semantic map to coco
		runwayCoco.get("output", CocoImage);//get final image from coco
	}
	
}

//--------------------------------------------------------------
void ofApp::draw(){
	//draw image recieved from deeplab and from coco
	DeeplabImage.draw(0, 0);
	CocoImage.draw(650, 0);
	
}

//--------------------------------------------------------------
// Runway sends information about the current model
//--------------------------------------------------------------
void ofApp::runwayInfoEvent(ofJson& info) {
	ofLogNotice("ofApp::runwayInfoEvent") << info.dump(2);
}
// if anything goes wrong
//--------------------------------------------------------------
void ofApp::runwayErrorEvent(string& message) {
	ofLogNotice("ofApp::runwayErrorEvent") << message;
}
